console-log (1.2-2.0) UNRELEASED; urgency=medium

  [ Marc Haber ]
  * NOT YET RELEASED

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 01 Oct 2017 23:48:40 +0200

console-log (1.2-2) unstable; urgency=low

  * declare dependency on lsb-base
  * drop versioned dependency on initscripts (Closes: #804956)
  * Standards-Version: 4.1.0 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 01 Oct 2017 21:39:29 +0000

console-log (1.2-1) unstable; urgency=low

  * make logpager configurable, add pager-specific configuration.
  * re-work docs, include docs for pager-specific configuration
  * this now allowes re-reading of a rotated log (Closes: #111272)
  * remove /usr/local from local path.
  * use long options for less (jless supports them now)
  * debhelper 9, package format 3.0 quilt
  * move /var/run to /run
  * move default home directory of account to /nonexistent
  * machine readable copyright file
  * change license to GPL2+
  * add Vcs and Homepage fields
  * make short package description clearer
  * remove exim 3's /var/log/exim/mainlog from default config
  * remove TODO file
  * Standards-Version: 3.9.5 (no other changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 24 Nov 2013 14:34:18 +0100

console-log (1.1-2) unstable; urgency=low

  * Stop postrm from failing in some obscure synthetic test environments.
    Closes: #604223
  * Standards-Versions: 3.9.1 (no changes needed)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 21 Nov 2010 17:11:12 +0100

console-log (1.1-1) unstable; urgency=low

  * remove debconf. Thanks to Christian Perrier. Closes: #578258
  * The upstream version bump is necessary because of the
    debian/console-log.templates file in the .orig.tar.gz
  * remove convert-console-log.conf
  * Standards-Version: 3.9.0 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Tue, 29 Jun 2010 21:21:38 +0200

console-log (1.0-17) unstable; urgency=low

  * The "Happy Birthday Sandra" upload. Thanks to bubulle.
  * Add Russian (ru) debconf messages.
    Thanks to Yuri Kozlov. Closes: #545218
  * Add debian/source/format
  * Standards-Version. 3.8.4 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 18 Apr 2010 12:47:56 +0200

console-log (1.0-16) unstable; urgency=low

  * Add Brazilian Portuguese (pt_BR) debconf messages.
    Thanks to Flamarion Jorge. Closes: #512455
  * Standards-Version: 3.8.1 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 07 Jun 2009 15:05:07 +0200

console-log (1.0-15) unstable; urgency=low

  * Fix wrong logic in /lib/lsb/init-functions processing.
  * Standards-Version: 3.8.0 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 21 Jun 2008 16:17:52 +0200

console-log (1.0-14) unstable; urgency=low

  * fix typo in CONLOGDEBUG varaiable name
  * Invoke ulimit stuff in a subshell.
    Thanks to Martin Völlinger. Closes: #431006
  * Use debian/compat instead of DH_COMPAT
  * Build-Depend on debhelper 5

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 12 Aug 2007 21:38:31 +0200

console-log (1.0-13) unstable; urgency=low

  * Add Portuguese (pt) debconf messages.
    Thanks to Joao Estevao and Rui Branco. Closes: #378886
  * create /var/run/console-log in init script. Closes: #390153
  * Remove console-tools from Depends:. Closes: #387125
  * Fix postrm to issue a correct deluser call and to gracefully handle
    deluser absence. Thanks to Bill Allombert. Closes: #389351
  * lsb-ize init script. Closes: #377026
  * Wiggle around build-depends until lintian is happy
  * Standards-Version: 3.7.2 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon,  2 Oct 2006 08:54:24 +0000

console-log (1.0-12) unstable; urgency=low

  * Add Spanish (es) debconf messages.
    Thanks to César Gómez Martín. Closes: #334553.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Fri, 18 Nov 2005 12:43:51 +0000

console-log (1.0-11) unstable; urgency=low

  * Add Czech (cz) debconf messages. Thanks to Martin Sin. Closes: #306444.
  * Add Vietnamese (ci) debconf messages.
    Thanks to Clytie Siddall. Closes: #310061
  * Add Swedish (sv) debconf messages.
    Thanks to Daniel Nylander. Closes: #333481
  * fix syntax error in changelog.
  * eliminate possible bashism ([ .. -a .. ]) from console-log.config.
  * Standards-Version: 3.6.2 (no changes needed)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Wed, 12 Oct 2005 11:41:08 +0000

console-log (1.0-10) unstable; urgency=low

  * fix find options in init.d script. Thanks to Andreas Metzler.
    Closes: #280226.
  * fix typo in console-log.conf. Thanks to Martin Schwarz.
    Closes: #284810.
  * add Italian debconf messages. Thanks to Luca Monducci. Closes: #277928.
  * introduce maxfilesize option. If a file to be pages exceeds the
    configured size, no pager is started.
  * set ulimit -v for the pager. Closes: #280528.
  * invoke debconf-updatepo in debian/rules clean.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Thu, 16 Dec 2004 15:51:03 +0000

console-log (1.0-9) unstable; urgency=low

  * handle/ignore openvt errors to fail gracefully on headless systems.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun,  8 Aug 2004 09:55:42 +0000

console-log (1.0-8) unstable; urgency=low

  * add "no upstream" clause in debian/control
  * add README.Debian-accountname
  * add German debconf messages. Thanks to Erik Schanze. Closes: #251106.
  * modify init script and logpager to invoke less with -Pw and allow
    more pager flexibility. Closes: #254078.
  * use dh_installman instead of dh_installmanpages

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon, 26 Jul 2004 15:07:30 +0000

console-log (1.0-7) unstable; urgency=low

  * adapt console-log.conf man page to the current situation, add a
    paragraph babbling about permission issues with exim 3's log
    files (see also #245516), fix broken formatting.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon, 26 Apr 2004 10:35:54 +0000

console-log (1.0-6) unstable; urgency=low

  * do not try to invoke pager if file is not readable by the target
    user. Closes: #245321.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon, 26 Apr 2004 09:26:28 +0000

console-log (1.0-5) unstable; urgency=low

  * add Dutch debconf messages. Thanks to Luk Claes. Closes: #241432.
  * add || true to unset found in init script. This has caused
    failures on woody.
  * do not try to invoke pager if file is not readable by the target
    user. Closes: #245321.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Tue, 13 Apr 2004 19:48:32 +0000

console-log (1.0-4) unstable; urgency=low

  * add Danish debconf messages. Thanks to Claus Hindsgaul. Closes: #236860.
  * add Japanese debconf messages. Thanks to Hideki Yamane. Closes: #227201.
  * fix two typo in console-log.conf.pod. Closes: #229956.
    Thanks to Andreas Metzler.
  * change default config to /var/log/exim4/mainlog
  * Create user account in postinst and run logpager with that account.
    Versioned depend on shadow, because earlier addgroup doesn't like
    the group name. Closes: #226461.
  * Depend on console-tools | kbd. Thanks to Adeodato Simó. Closes: #230683.
  * Allow multiple files in the file line, less the first file found.
    Adapt docs. New /etc/console-log.conf with paths to exim, exim4,
    sendmail and postfix log files. Closes: #240461.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Tue, 30 Mar 2004 08:52:56 +0000

console-log (1.0-3) unstable; urgency=low

  * add french debconf template from Christian Perrier (Closes: Bug#207053)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon, 25 Aug 2003 06:00:21 +0000

console-log (1.0-2) unstable; urgency=low

  * fix typo in manpage (Closes: Bug#171358).
  * switch to gettext-based debconf templates by taking the Patch from
    Christian Perrier (Closes: Bug#205768)
  * have init script check for pager presence by taking the Patch from
    Dan Torop (Closes: Bug#206926)
  * make console-log.copyright copyright again, fix typo in license path.
  * Standards-Version: 3.6.1

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 24 Aug 2003 10:42:22 +0000

console-log (1.0-1) unstable; urgency=low

  * no bug reports for 0.8-1 for a month, it's time for a release.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Thu, 28 Nov 2002 17:48:17 +0000

console-log (0.8-1) unstable; urgency=low

  * is not a Debian native package any more.
  * introduce new config file format.
  * include conversion script.
  * give debconf warning on installation.
  * use invoke-rc.d in maintainer scripts (Closes: Bug#162694).
  * console-log.local replaced with individual local files.
  * new init script gives +F again (Closes: Bug#162640).
  * This version is considered a pre-release for a 1.0 version.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 26 Oct 2002 09:38:03 +0000

console-log (0.7) unstable; urgency=low

  * ditch run, use daemon.
  * introduce /etc/console-log.local to allow the local admin to set
    environment variables for the logpager (Closes: Bug#158981).
  * move /usr/lib/console-log/logpager to /usr/share/console-log.
  * include man pages for the config files.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon, 23 Sep 2002 10:18:54 +0000

console-log (0.6) unstable; urgency=low

  * Standards 3.5.6
  * Upload with my own signature, unsponsored now.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun,  2 Sep 2001 22:39:45 +0200

console-log (0.5) unstable; urgency=low

  * fixed init.d script breaking with sparse environment.
  * init.d script is now a bash script (Closes: Bug#87424).
  * added Build-Depends:.
  * Fixed quoting bug in init.d script (Closes: Bug#87641).
  * Made chvt optional (Closes: Bug#88983).
  * terminal is now cleared with TERM=vt100. This should work on
    the Linux console and nearly everywhere else.

 -- Marc Haber <debian-packages@marc-haber.de>  Thu,  1 Mar 2001 11:38:08 +0100

console-log (0.4) unstable; urgency=low

  * modified pager scripts to use short options because jless doesn't
    have long options (closes: Bug#64444)
  * console-log now Depends: on run (>= 0.9.2-4). (closes: Bug#75215)
    (closes: Bug#79050) (closes: Bug#64295) (closes: Bug#68398)
  * bashism removed from init script (closes: Bug#66821)
  * now starts in postinst
  * now stops in prerm
  * remove bashism "function" from init script
  * implement config file (closes: Bug#77654) (closes: Bug#71281).
    Generating that config file during installation is left as exercise
    to the reader. Debconf would be nice ;)
  * is now an architecture: all package

 -- Marc Haber <debian-packages@marc-haber.de>  Sat, 24 Jan 2001 13:58:31 +0100

console-log (0.3) unstable; urgency=low

  * applied patch given by Roman Hodek to make package compile on m68k
    (closes: Bug#64136)

 -- Marc Haber <debian-packages@marc-haber.de>  Mon, 15 May 2000 13:54:43 +0200

console-log (0.2) unstable; urgency=low

  * clear consoles after stop
  * set LESSSECURE to prevent shell escapes

 -- Marc Haber <debian-packages@marc-haber.de>  Wed, 26 Apr 2000 18:19:44 +0200

console-log (0.1) unstable; urgency=low

  * Initial release

 -- Marc Haber <debian-packages@marc-haber.de>  Wed, 26 Apr 2000 10:15:30 +0200
