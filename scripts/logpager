#!/bin/bash

PATH=${logpager_path:-/sbin:/bin:/usr/sbin:/usr/bin}

PAGER="$1"
FILENAME="$2"
MAXFILESIZE="$3"

FILESIZE="$(stat --format="%s" "$FILENAME")"
if [ "$FILESIZE" -gt "$MAXFILESIZE" ]; then
    echo >&2 "ERR: $FILENAME size $FILESIZE exceeds maximum $MAXFILESIZE, not starting pager!"
    sleep 3600
    exit 1
fi

# the pager_foo variables need to be in sync with the init script
# which is responsible to unset the variables first before parsing config

case "$PAGER" in
  less)
    LESSSECURE="${less_lesssecure:-1}"
    export LESSSECURE

    LESS="${less_opts:---LONG-PROMPT --chop-long-lines --jump-target=10 --max-forw-scroll=100 --window=-4}"
    export LESS
    LESSKEY="${less_lesskey:-/usr/lib/console-log/lesskey}"
    if [ ! -e "$LESSKEY" ]; then
      unset LESSKEY
    else
      export LESSKEY
    fi
    TERM="${less_term:-linux}"
    export TERM

    reset
    exec less -Pw"less $FILENAME" +F $FILENAME
    ;;
  *)
    echo >&2 "ERR: pager $PAGER not supported!"
    exit 1
    ;;
esac
